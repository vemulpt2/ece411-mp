import lc3b_types::*; /* Import types defined in lc3b_types.sv */

module control
(
    /* Input and output port declarations */
	 input clk, mem_resp,
	 output logic pcmux_sel,
    output logic load_pc,
	 output logic storemux_sel,
	 output logic load_regfile,
	 output logic load_ir,
	 output logic marmux_sel,
	 output logic load_mar,
	 output logic mdrmux_sel,
	 output logic load_mdr,
	 output logic load_cc,
	 output logic alumux_sel,
	 output logic regfilemux_sel,
	 output lc3b_aluop aluop,
	 input branch_enable,
	 
	 input lc3b_opcode opcode,
	 
	 output logic mem_read, mem_write,
	 output lc3b_mem_wmask mem_byte_enable
);

enum int unsigned {
    /* List of states */
	 FETCH1, FETCH2, FETCH3, DECODE,
	 ADD, AND, NOT,
	 CALC_ADDR, LDR1, LDR2, STR1, STR2,
	 BR, BR_TAKEN
} state, next_state;

always_comb
begin : state_actions
    /* Default output assignments */
    
	 load_pc = 0;
	 load_mar = 0;
	 load_mdr = 0;
	 load_ir = 0;
	 load_regfile = 0;
	 load_cc = 0;
	 pcmux_sel = 0;
	 storemux_sel = 0;
	 alumux_sel = 0;
	 regfilemux_sel = 0;
	 marmux_sel = 0;
	 mdrmux_sel = 0;
	 aluop = alu_add;
	 mem_read = 0;
	 mem_write = 0;
	 mem_byte_enable = 2'b11;
	 /* Actions for each state */
	 case(state)
	 
		FETCH1: begin
			marmux_sel = 1;
			load_mar = 1;
			pcmux_sel = 0;
			load_pc = 1;
		end
		
		FETCH2: begin
			mdrmux_sel = 1;
			load_mdr = 1;
			mem_read = 1;
		end
		
		FETCH3: load_ir = 1;
		ADD: begin
			aluop = alu_add;
			load_regfile = 1;
			load_cc = 1;
		end
		
		AND: begin
			aluop = alu_and;
			load_regfile = 1;
			load_cc = 1;
		end
		
		NOT: begin
			aluop = alu_not;
			load_regfile = 1;
			load_cc = 1;
		end
		
		BR_TAKEN: begin
			pcmux_sel = 1;
			load_pc = 1;
		end
		
		CALC_ADDR: begin
			alumux_sel = 1;
			aluop = alu_add;
			load_mar = 1;
		end
		
		LDR1: begin
			mdrmux_sel = 1;
			load_mdr = 1;
			mem_read = 1;
		end
		
		LDR2: begin
			regfilemux_sel = 1;
			load_regfile = 1;
			load_cc = 1;
		end	
		
		STR1: begin
			storemux_sel = 1;
			aluop = alu_pass;
			load_mdr = 1;
		end
		STR2: begin
			mem_write = 1;
		end
		default: ;
	 endcase
end

always_comb
begin : next_state_logic
    
	 next_state = state;
	 
	 unique case(state)
	 FETCH1: next_state = FETCH2;
	 
	 FETCH2: 
			if(mem_resp)
				next_state = FETCH3;
				
	 FETCH3: next_state = DECODE;
	 
	 DECODE: begin
		case(opcode)
			op_add: next_state = ADD;
			op_and: next_state = AND;
			op_not: next_state = NOT;
			op_ldr, op_str: next_state = CALC_ADDR;
			op_br: next_state = BR;
			default: next_state = FETCH1;
		endcase
	 end
	 
	 CALC_ADDR:
		if(opcode == op_ldr)
			next_state = LDR1;
		else
			next_state = STR1;
			
	LDR1: begin
		if(mem_resp)
			next_state = LDR2;
	end
			
	STR1: next_state = STR2;
	
	STR2: begin
		if(mem_resp)
			next_state = FETCH1;
	end
	
	BR: begin
		if(branch_enable)
			next_state = BR_TAKEN;
		else
			next_state = FETCH1;
	end
	default: next_state = FETCH1;
	endcase
	
end

always_ff @(posedge clk)
begin: next_state_assignment
    state <= next_state;
end

endmodule : control
