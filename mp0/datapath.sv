import lc3b_types::*;

module datapath
(
    input clk,

    /* control signals */
    input pcmux_sel,
    input load_pc,
	 input storemux_sel,
	 input load_regfile,
	 input load_ir,
	 input marmux_sel,
	 input load_mar,
	 input mdrmux_sel,
	 input load_mdr,
	 input load_cc,
	 input alumux_sel,
	 input lc3b_word mem_rdata,
	 input regfilemux_sel,
	 input lc3b_aluop aluop,
	 output logic branch_enable,
	 output lc3b_word mem_address, mem_wdata,
	 output lc3b_opcode opcode
	 
	 

    /* declare more ports here */
);

/* declare internal signals */
lc3b_word pcmux_out;
lc3b_word pc_out;
lc3b_word br_add_out;
lc3b_word pc_plus2_out;
lc3b_word marmux_out;
lc3b_word mdrmux_out;
lc3b_word regfilemux_out;
lc3b_word sext9_out, sext6_out;
lc3b_word alu_out, alumux_out;
lc3b_word sr1_out, sr2_out;


lc3b_reg sr1, sr2;
lc3b_reg dest;
lc3b_reg storemux_out;


/* IR*/
lc3b_offset9 offset9;
lc3b_offset6 offset6;

ir ir (.*, .load(load_ir), .in(mem_wdata));

/*
 * PC
 */
adj #(9) adj9 (.in(offset9), .out(sext9_out));

br_adder br_adder
(
	.in(sext9_out),
	.pc(pc_out),
	.out(br_add_out)
);

plus2 plus2
(
	.in(pc_out),
	.out(pc_plus2_out)
);

mux2 pcmux
(
    .sel(pcmux_sel),
    .a(pc_plus2_out),
    .b(br_add_out),
    .f(pcmux_out)
);

register pc
(
    .clk,
    .load(load_pc),
    .in(pcmux_out),
    .out(pc_out)
);

/* MAR */
mux2 marmux
(
	.sel(marmux_sel),
	.a(alu_out),
	.b(pc_out),
	.f(marmux_out)
);

register MAR
(
	.clk, .load(load_mar), .in(marmux_out), 
	.out(mem_address)
);

/* MDR*/
mux2 mdrmux
(
	.sel(mdrmux_sel),
	.a(alu_out),
	.b(mem_rdata),
	.f(mdrmux_out)
);

register MDR
(
	.clk,  .load(load_mdr), .in(mdrmux_out), 
	.out(mem_wdata)
);

/* Regfile */
mux2 #(.width(3)) storemux
(
	.sel(storemux_sel),
	.a(sr1),
	.b(dest),
	.f(storemux_out)
);



regfile regfile
(
    .clk(clk),
    .load(load_regfile),
    .in(regfilemux_out),
    .src_a(storemux_out), .src_b(sr2), .dest(dest),
    .reg_a(sr1_out), .reg_b(sr2_out)
);

mux2 regfile_mux
(
	.sel(regfilemux_sel),
	.a(alu_out),
	.b(mem_wdata),
	.f(regfilemux_out)
);

/* ALU*/
adj #(6) adj6 (.in(offset6), .out(sext6_out));

mux2 alumux
(
	.sel(alumux_sel),
	.a(sr2_out),
	.b(sext6_out),
	.f(alumux_out)
);

alu alu
(
    .aluop,
    .a(sr1_out), .b(alumux_out),
    .f(alu_out)
);


/* Cc */
lc3b_nzp gencc_out, cc_out;

gencc gencc(.in(regfilemux_out), .out(gencc_out));

register #(3) cc
(
	.clk, .load(load_cc),
	.in(gencc_out), .out(cc_out)
);

cccomp cccomp (.cc_in(cc_out), .dest, .branch_enable);


endmodule : datapath
