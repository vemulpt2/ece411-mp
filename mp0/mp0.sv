import lc3b_types::*;

module mp0
(
    input clk,

    /* Memory signals */
    input mem_resp,
    input lc3b_word mem_rdata,
    output mem_read,
    output mem_write,
    output lc3b_mem_wmask mem_byte_enable,
    output lc3b_word mem_address,
    output lc3b_word mem_wdata
);

/* Instantiate MP 0 top level blocks here */

 /* Internal signals between datapath and controller */
 logic pcmux_sel;
 logic load_pc;
 logic storemux_sel;
 logic load_regfile;
 logic load_ir;
 lc3b_aluop aluop;
 logic marmux_sel;
 logic load_mar;
 logic mdrmux_sel;
 logic load_mdr;
 logic load_cc;
 logic alumux_sel;
 logic regfilemux_sel;
 logic branch_enable;
 
 lc3b_opcode opcode;
 
 datapath datapath (.*);
 control control (.*);
 


endmodule : mp0
