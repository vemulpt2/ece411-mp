onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label clk -radix hexadecimal /mp0_tb/clk
add wave -noupdate -label pc_out -radix hexadecimal /mp0_tb/dut/datapath/pc/data
add wave -noupdate -label mem_address -radix hexadecimal /mp0_tb/mem_address
add wave -noupdate -label mem_read -radix hexadecimal /mp0_tb/mem_read
add wave -noupdate -label mem_rdata -radix hexadecimal /mp0_tb/mem_rdata
add wave -noupdate -label mem_write -radix hexadecimal /mp0_tb/mem_write
add wave -noupdate -label mem_byte_enable -radix hexadecimal /mp0_tb/mem_byte_enable
add wave -noupdate -label mem_wdata -radix hexadecimal /mp0_tb/mem_wdata
add wave -noupdate -label regfile -radix hexadecimal -childformat {{{/mp0_tb/dut/datapath/regfile/data[7]} -radix hexadecimal} {{/mp0_tb/dut/datapath/regfile/data[6]} -radix hexadecimal} {{/mp0_tb/dut/datapath/regfile/data[5]} -radix hexadecimal} {{/mp0_tb/dut/datapath/regfile/data[4]} -radix hexadecimal} {{/mp0_tb/dut/datapath/regfile/data[3]} -radix hexadecimal} {{/mp0_tb/dut/datapath/regfile/data[2]} -radix hexadecimal} {{/mp0_tb/dut/datapath/regfile/data[1]} -radix hexadecimal} {{/mp0_tb/dut/datapath/regfile/data[0]} -radix hexadecimal}} -expand -subitemconfig {{/mp0_tb/dut/datapath/regfile/data[7]} {-radix hexadecimal} {/mp0_tb/dut/datapath/regfile/data[6]} {-radix hexadecimal} {/mp0_tb/dut/datapath/regfile/data[5]} {-radix hexadecimal} {/mp0_tb/dut/datapath/regfile/data[4]} {-radix hexadecimal} {/mp0_tb/dut/datapath/regfile/data[3]} {-radix hexadecimal} {/mp0_tb/dut/datapath/regfile/data[2]} {-radix hexadecimal} {/mp0_tb/dut/datapath/regfile/data[1]} {-radix hexadecimal} {/mp0_tb/dut/datapath/regfile/data[0]} {-radix hexadecimal}} /mp0_tb/dut/datapath/regfile/data
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {199050 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {210 ns}
