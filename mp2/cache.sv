import lc3b_types::*;
import cache_types::*;

module cache
(
	input clk,

    /* Memory signals */
    output logic mem_resp,
    output lc3b_word mem_rdata,
    input mem_read,
    input mem_write,
    input lc3b_mem_wmask mem_byte_enable,
    input lc3b_word mem_address,
    input lc3b_word mem_wdata,
	 output lc3b_word pmem_address,
	 input pmem_bus pmem_rdata,
	 output pmem_bus pmem_wdata,
	 input pmem_resp,
	 output pmem_read, pmem_write
);


logic valid_in, cache_allocate;
logic datain_mux_sel, write_enable, cache_hit;
logic dirty_datain, pmem_address_sel;
logic dirtyout;
cache_datapath cache_datapath(.*);
cache_control cache_control(.*);



endmodule: cache