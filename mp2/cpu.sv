import lc3b_types::*;

module cpu
(
    input clk,

    /* Memory signals */
    input mem_resp,
    input lc3b_word mem_rdata,
    output mem_read,
    output mem_write,
    output lc3b_mem_wmask mem_byte_enable,
    output lc3b_word mem_address,
    output lc3b_word mem_wdata
);

/* Instantiate MP 0 top level blocks here */

 /* Internal signals between datapath and controller */
 logic [1:0] pcmux_sel;
 logic load_pc;
 logic storemux_sel;
 logic load_regfile;
 logic load_ir;
 lc3b_aluop aluop;
 logic [1:0] marmux_sel;
 logic load_mar;
 logic [1:0] mdrmux_sel;
 logic load_mdr;
 logic load_cc;
 logic [1:0] alumux_sel;
 logic [1:0] regfilemux_sel, ldbmux_sel;
 logic branch_enable, addr_lsb;
 logic bit5, bit4;
 lc3b_opcode opcode;
 logic destmux_sel;
 
 cpu_datapath cpu_datapath (.*);
 cpu_control cpu_control (.*);
 


endmodule : cpu
