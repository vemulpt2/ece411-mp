import lc3b_types::*;

module cpu_datapath
(
    input clk,

    /* control signals */
    input [1:0] pcmux_sel,
    input load_pc,
	 input storemux_sel,
	 input load_regfile,
	 input load_ir,
	 input [1:0] marmux_sel,
	 input load_mar,
	 input [1:0] mdrmux_sel,
	 input load_mdr,
	 input load_cc,
	 input [1:0] alumux_sel,
	 input lc3b_word mem_rdata,
	 input [1:0]regfilemux_sel, ldbmux_sel,
	 input lc3b_aluop aluop,
	 input destmux_sel,
	 output logic branch_enable,
	 output lc3b_word mem_address, mem_wdata,
	 output lc3b_opcode opcode,
	 output logic bit5, bit4, addr_lsb

	 
);

/* declare internal signals */
lc3b_word pcmux_out;
lc3b_word pc_out;
lc3b_word br_add_out;
lc3b_word pc_plus2_out;
lc3b_word marmux_out;
lc3b_word mdrmux_out;
lc3b_word regfilemux_out;
lc3b_word adj11_out, jsrmux_out;
lc3b_word adj9_out, adj6_out;
lc3b_word alu_out, alumux_out;
lc3b_word sr1_out, sr2_out;
lc3b_word sext5_out;

lc3b_reg sr1, sr2;
lc3b_reg dest;
lc3b_reg storemux_out;
lc3b_reg destmux_out;

lc3b_word ldbmux_out;
lc3b_word offset6mux_out;
lc3b_word sext6_out;
lc3b_word trapadj8_out;

/* IR*/
lc3b_offset9 offset9;
lc3b_offset6 offset6;
lc3b_offset11 offset11;
lc3b_imm5 imm5;
lc3b_imm4 imm4;

lc3b_trapvect8 trapvect8;
logic bit11;

assign addr_lsb = mem_address[0];

ir ir (.*, .load(load_ir), .in(mem_wdata));

/*
 * PC
 */
adj #(9) adj9 (.in(offset9), .out(adj9_out));
adj #(11) adj11 (.in(offset11), .out(adj11_out));

mux2 jsrmux
(
	.sel(bit11),
	.a(alu_out),
	.b(adj11_out + pc_out),
	.f(jsrmux_out)
);

br_adder br_adder
(
	.in(adj9_out),
	.pc(pc_out),
	.out(br_add_out)
);

plus2 plus2
(
	.in(pc_out),
	.out(pc_plus2_out)
);

mux4 pcmux
(
    .sel(pcmux_sel),
    .a(pc_plus2_out),
    .b(br_add_out),
	 .c(jsrmux_out),
	 .d(mem_wdata),
    .f(pcmux_out)
);

register pc
(
    .clk,
    .load(load_pc),
    .in(pcmux_out),
    .out(pc_out)
);

/* MAR */
trapadj trapadj8 ( .in(trapvect8), .out(trapadj8_out));

mux4 marmux
(
	.sel(marmux_sel),
	.a(alu_out),
	.b(pc_out),
	.c(mem_rdata),
	.d(trapadj8_out),
	.f(marmux_out)
);

register MAR
(
	.clk, .load(load_mar), .in(marmux_out), 
	.out(mem_address)
);

/* MDR*/
mux4 mdrmux
(
	.sel(mdrmux_sel),
	.a(alu_out),
	.b(mem_rdata),
	.c({8'b0,alu_out[7:0]}),
	.d({alu_out[7:0], 8'b0}),
	.f(mdrmux_out)
);

register MDR
(
	.clk,  .load(load_mdr), .in(mdrmux_out), 
	.out(mem_wdata)
);

mux4 ldbmux
(
	.sel(ldbmux_sel),
	.a(mem_wdata),
	.b(mem_wdata),
	.c({8'h0,mem_wdata[7:0]}),
	.d({8'h0, mem_wdata[15:8]}),
	
	.f(ldbmux_out)
);
/* Regfile */
mux2 #(.width(3)) storemux
(
	.sel(storemux_sel),
	.a(sr1),
	.b(dest),
	.f(storemux_out)
);

mux2 #(3) destmux
(
	.sel(destmux_sel),
	.a(dest),
	.b(3'b111),
	.f(destmux_out)
);

regfile regfile
(
    .clk(clk),
    .load(load_regfile),
    .in(regfilemux_out),
    .src_a(storemux_out), .src_b(sr2), .dest(destmux_out),
    .reg_a(sr1_out), .reg_b(sr2_out)
);

mux4 regfile_mux
(
	.sel(regfilemux_sel),
	.a(alu_out),
	.b(ldbmux_out),
	.c(br_add_out),
	.d(pc_out),
	.f(regfilemux_out)
);

/* ALU*/
adj #(6) adj6 (.in(offset6), .out(adj6_out));

sext #(6) sext6(.in(offset6), .out(sext6_out));
sext #(5) sext5 (.in(imm5), .out(sext5_out));

/* Mux to choose between shifted or non shifted for  LDBmu*/
mux2 offset6mux
(
	.sel((opcode == op_ldb || opcode == op_stb)),
	.a(adj6_out),
	.b(sext6_out),
	.f(offset6mux_out)
);

mux4 alumux
(
	.sel(alumux_sel),
	.a(sr2_out),
	.b(offset6mux_out),
	.c(sext5_out),
	.d({12'b0, imm4}),
	.f(alumux_out)
);

alu alu
(
    .aluop,
    .a(sr1_out), .b(alumux_out),
    .f(alu_out)
);


/* Cc */
lc3b_nzp gencc_out, cc_out;

gencc gencc(.in(regfilemux_out), .out(gencc_out));

register #(3) cc
(
	.clk, .load(load_cc),
	.in(gencc_out), .out(cc_out)
);

cccomp cccomp (.cc_in(cc_out), .dest, .branch_enable);


endmodule : cpu_datapath
