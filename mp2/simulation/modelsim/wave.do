onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label tag -radix hexadecimal /mp2_tb/dut/cache/cache_datapath/tag
add wave -noupdate -label index -radix hexadecimal /mp2_tb/dut/cache/cache_datapath/tag_array1/index
add wave -noupdate -label offset -radix hexadecimal /mp2_tb/dut/cache/cache_datapath/offset
add wave -noupdate -radix hexadecimal -childformat {{{/mp2_tb/dut/cache/cache_datapath/tag_array0/data[7]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/tag_array0/data[6]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/tag_array0/data[5]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/tag_array0/data[4]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/tag_array0/data[3]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/tag_array0/data[2]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/tag_array0/data[1]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/tag_array0/data[0]} -radix hexadecimal}} -subitemconfig {{/mp2_tb/dut/cache/cache_datapath/tag_array0/data[7]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/tag_array0/data[6]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/tag_array0/data[5]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/tag_array0/data[4]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/tag_array0/data[3]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/tag_array0/data[2]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/tag_array0/data[1]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/tag_array0/data[0]} {-height 16 -radix hexadecimal}} /mp2_tb/dut/cache/cache_datapath/tag_array0/data
add wave -noupdate -radix hexadecimal -childformat {{{/mp2_tb/dut/cache/cache_datapath/tag_array1/data[7]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/tag_array1/data[6]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/tag_array1/data[5]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/tag_array1/data[4]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/tag_array1/data[3]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/tag_array1/data[2]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/tag_array1/data[1]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/tag_array1/data[0]} -radix hexadecimal}} -subitemconfig {{/mp2_tb/dut/cache/cache_datapath/tag_array1/data[7]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/tag_array1/data[6]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/tag_array1/data[5]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/tag_array1/data[4]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/tag_array1/data[3]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/tag_array1/data[2]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/tag_array1/data[1]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/tag_array1/data[0]} {-height 16 -radix hexadecimal}} /mp2_tb/dut/cache/cache_datapath/tag_array1/data
add wave -noupdate -radix hexadecimal -childformat {{{/mp2_tb/dut/cache/cache_datapath/data_array1/data[7]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/data_array1/data[6]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/data_array1/data[5]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/data_array1/data[4]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/data_array1/data[3]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/data_array1/data[2]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/data_array1/data[1]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/data_array1/data[0]} -radix hexadecimal}} -subitemconfig {{/mp2_tb/dut/cache/cache_datapath/data_array1/data[7]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/data_array1/data[6]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/data_array1/data[5]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/data_array1/data[4]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/data_array1/data[3]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/data_array1/data[2]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/data_array1/data[1]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/data_array1/data[0]} {-height 16 -radix hexadecimal}} /mp2_tb/dut/cache/cache_datapath/data_array1/data
add wave -noupdate -radix hexadecimal -childformat {{{/mp2_tb/dut/cache/cache_datapath/data_array0/data[7]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/data_array0/data[6]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/data_array0/data[5]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/data_array0/data[4]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/data_array0/data[3]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/data_array0/data[2]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/data_array0/data[1]} -radix hexadecimal} {{/mp2_tb/dut/cache/cache_datapath/data_array0/data[0]} -radix hexadecimal}} -subitemconfig {{/mp2_tb/dut/cache/cache_datapath/data_array0/data[7]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/data_array0/data[6]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/data_array0/data[5]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/data_array0/data[4]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/data_array0/data[3]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/data_array0/data[2]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/data_array0/data[1]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cache/cache_datapath/data_array0/data[0]} {-height 16 -radix hexadecimal}} /mp2_tb/dut/cache/cache_datapath/data_array0/data
add wave -noupdate -radix hexadecimal /mp2_tb/dut/cache/cache_datapath/valid_array0/data
add wave -noupdate -radix hexadecimal /mp2_tb/dut/cache/cache_datapath/valid_array1/data
add wave -noupdate -radix hexadecimal /mp2_tb/dut/cache/cache_datapath/dirty_array0/data
add wave -noupdate -radix hexadecimal /mp2_tb/dut/cache/cache_datapath/dirty_array1/data
add wave -noupdate -radix hexadecimal /mp2_tb/dut/cache/dirtyout
add wave -noupdate -radix hexadecimal /mp2_tb/dut/cache/dirty_datain
add wave -noupdate -radix hexadecimal /mp2_tb/dut/cache/cache_hit
add wave -noupdate -radix hexadecimal /mp2_tb/dut/cache/cache_datapath/lru_dataout
add wave -noupdate -radix hexadecimal /mp2_tb/dut/cache/cache_datapath/way_match
add wave -noupdate -radix hexadecimal -childformat {{{/mp2_tb/dut/cpu/cpu_datapath/regfile/data[7]} -radix hexadecimal} {{/mp2_tb/dut/cpu/cpu_datapath/regfile/data[6]} -radix hexadecimal} {{/mp2_tb/dut/cpu/cpu_datapath/regfile/data[5]} -radix hexadecimal} {{/mp2_tb/dut/cpu/cpu_datapath/regfile/data[4]} -radix hexadecimal} {{/mp2_tb/dut/cpu/cpu_datapath/regfile/data[3]} -radix hexadecimal} {{/mp2_tb/dut/cpu/cpu_datapath/regfile/data[2]} -radix hexadecimal} {{/mp2_tb/dut/cpu/cpu_datapath/regfile/data[1]} -radix hexadecimal} {{/mp2_tb/dut/cpu/cpu_datapath/regfile/data[0]} -radix hexadecimal}} -expand -subitemconfig {{/mp2_tb/dut/cpu/cpu_datapath/regfile/data[7]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cpu/cpu_datapath/regfile/data[6]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cpu/cpu_datapath/regfile/data[5]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cpu/cpu_datapath/regfile/data[4]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cpu/cpu_datapath/regfile/data[3]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cpu/cpu_datapath/regfile/data[2]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cpu/cpu_datapath/regfile/data[1]} {-height 16 -radix hexadecimal} {/mp2_tb/dut/cpu/cpu_datapath/regfile/data[0]} {-height 16 -radix hexadecimal}} /mp2_tb/dut/cpu/cpu_datapath/regfile/data
add wave -noupdate -radix hexadecimal /mp2_tb/dut/cpu/cpu_datapath/pc/out
add wave -noupdate -radix hexadecimal /mp2_tb/dut/cpu/mem_resp
add wave -noupdate -radix hexadecimal /mp2_tb/dut/cpu/mem_rdata
add wave -noupdate -radix hexadecimal /mp2_tb/dut/cpu/mem_read
add wave -noupdate -radix hexadecimal /mp2_tb/dut/cpu/mem_write
add wave -noupdate -radix hexadecimal /mp2_tb/dut/cpu/mem_byte_enable
add wave -noupdate -radix hexadecimal /mp2_tb/dut/cpu/mem_address
add wave -noupdate -radix hexadecimal /mp2_tb/dut/cpu/mem_wdata
add wave -noupdate -radix hexadecimal /mp2_tb/dut/cache/cache_control/state
add wave -noupdate -radix hexadecimal /mp2_tb/dut/cache/cache_control/cache_hit
add wave -noupdate -radix hexadecimal /mp2_tb/dut/cpu/cpu_control/state
add wave -noupdate /mp2_tb/dut/cache/cache_datapath/data_write_module/mem_byte_enable
add wave -noupdate /mp2_tb/dut/cache/cache_datapath/data_write_module/mem_wdata
add wave -noupdate /mp2_tb/dut/cache/cache_datapath/data_write_module/pmem_wdata
add wave -noupdate /mp2_tb/dut/cache/cache_datapath/data_write_module/offset
add wave -noupdate /mp2_tb/dut/cache/cache_datapath/data_write_module/data_writeout
add wave -noupdate /mp2_tb/dut/cache/cache_datapath/datain_mux/sel
add wave -noupdate /mp2_tb/dut/cache/cache_datapath/datain_mux/a
add wave -noupdate /mp2_tb/dut/cache/cache_datapath/datain_mux/b
add wave -noupdate /mp2_tb/dut/cache/cache_datapath/datain_mux/f
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2138246 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 209
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 2
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {1960666 ps} {2363456 ps}
