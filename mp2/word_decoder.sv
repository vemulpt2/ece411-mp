import lc3b_types::*;
import cache_types::*;

module word_decoder 
(
	input cache_offset offset,
	input pmem_bus datain,
	output lc3b_word dataout
);

always_comb
begin
/*
	case (offset)
	3'b000: dataout = datain[0+:16];
	3'b001: dataout = datain[16+:16];
	3'b010: dataout = datain[32+:16];
	3'b011: dataout = datain[48+:16];
	3'b100: dataout = datain[64+:16];
	3'b101: dataout = datain[80+:16];
	3'b110: dataout = datain[96+:16];
	3'b111: dataout = datain[112+:16];
	endcase
	*/
	dataout = datain[16*(offset) +:16 ];
end



endmodule: word_decoder

